
let printfn fmt = Printf.kprintf print_endline fmt
let tee f x = ignore(f x); x
let (<<) f g x = f (g x)

(* let y = 2 in let f = fun x -> x + y in let y = 100 in f 2 *)
let test code =
    code
    |> tee (printfn "%s\n")
    |> Lexing.from_string
    |> Parser.start Lexer.token 
    |> Machine.compile
    |> Machine.closure
    |> tee (print_endline << Machine.string_of_instrs)
    |> Machine.emit
    |> tee (printfn "len=%d\n" << List.length)
    |> Machine.string_of_instrs
    |> print_endline
    
(*"let rec fact = fun n -> if n = 0 then 1 else n * fact (n - 1) in fact 5" *)

(*
let () =
(*
"fun state gohst ->
  let rec step =
    fun cur field -> (cur + 1, 1)
  in (0, step)"
 *)
"let rec step = fun x y ->
   let y = dbug y in
   let next = if x >= 3 then 0 else x+1 in (next, x)
 in (1, step)"
|> test;
    "(fun x -> let (m,g) = x in atom atom (g,m)) (1,2)"
    |> test;
    "let x = (1,2,3) in let (a,b,c) = x in (b,a,c)"
    |> test;
    "let (a,b) = (1,2) in (b,a)"
    |> test;
    "let y = 2 in let f = fun x -> x + y in let y = 100 in f 2"
    |> test;
    "let rec fact = fun n -> if n = 0 then 1 else n * fact (n - 1) in fact 5"
    |> test;
    "let fact = fun n ->
      let rec loop = fun acc n ->
        if n = 1 then acc else let m = n - 1
        in loop (acc*m) m
      in loop n n in fact 5"
    |> test;
    "let rec fib = fun n ->
      if n <= 1 then n
      else fib (n - 1) + (fib (n - 2)) 
     in fib 30"
    |> test;
    "let rec ack = fun x y ->
         if x <= 0 then y + 1 else
         if y <= 0 then ack (x - 1) 1 else
         ack (x - 1) (ack x (y - 1)) in ack 3 10 :: 0"
    |> test
 *)
let () = 
  let verbose = ref false in
  let options = [
    "-v", Arg.Set verbose, "Verbose mode";
  ]
  in
  Arg.parse options (fun x -> ()) "Options: ";
  let src = Sys.argv.(1) in
  let compile oc lex =
    lex
    |> Parser.start Lexer.token 
    |> Machine.compile
    |> Machine.closure
    |> tee (print_endline << Machine.string_of_instrs)
    |> Machine.emit
    |> Machine.string_of_instrs
    |> output_string oc
  in
  let file f = (* ファイルをコンパイルしてファイルに出力する *)
    let base_fname = Filename.chop_suffix f ".ml" in
    let inchan = open_in f in
    let outchan = open_out (base_fname ^ ".gcc") in
    try
      compile outchan (Lexing.from_channel inchan);
      close_in inchan;
      close_out outchan;
    with e -> (close_in inchan; close_out outchan; raise e)
  in
  file src
