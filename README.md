Ebi-chan compiler (ebic)
------

Ebi-chan language is DSL for ICFPC2014 LambdaMan CPU (gcc).

It was made by OCaml, and it was not completed during the period of the contest.

###Feature
- Grammar is similar to ML.
- Not typed.
- Tail recursion.
- Purely functional. (not used ST instruction)
- There is no strategy to Solver. Just collect the pill simply.

- Ebi-chan is [エビちゃん].

### Build & compile solver
```sh
% ocamlbuild ebic.byte
% ocamlrun ebic.byte solver.ml
```

solver.gcc is created. and labeled source code during compilation is displayed to stdout.

Score
----
- Inital version `Score: 8000 Lives: 3 Ticks: 58981`

TODO
----
- **Solver**: Escape, if ghost is approached.
- **Solver**: Go to pick up fruit that appears.
- **Solver**: Go to catch up frightened ghost that appears.
- **Ebi lang**: Add a pattern match (match with syntax).
- **Ebi lang**: Add a Typing (need need).
- **Ebi lang**: Add a Logical operators (&&, ||).

Reference
----
* [min-caml] - Basic parts made based on this. very very useful site.
* [M.Hiroi's secd] - The implementation of the SECD machine by  Scheme. How to resolve local variable is very informative.
* [言語の実装は関数型言語で、は本当か] - Functional language is easy to make DSL. That was taught.
* [yoanncouillec-secd] - I got first inspiration.

License
----
MIT

[エビちゃん]:https://www.google.com/search?q=%E3%82%A8%E3%83%93%E3%81%A1%E3%82%83%E3%82%93&tbm=isch
[min-caml]:http://esumii.github.io/min-caml/
[M.Hiroi's secd]:http://www.geocities.jp/m_hiroi/func/abcscm33.html
[言語の実装は関数型言語で、は本当か]:http://d.hatena.ne.jp/camlspotter/20131105/1383619506
[yoanncouillec-secd]:https://github.com/yoanncouillec/secd
