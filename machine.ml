open Syntax

type instr =
  | LDC of int      (* load constant *)
  | LD of int * int (* load from environment *)
  | ADD             (* integer addition *)
  | SUB             (* integer subtraction *)
  | MUL             (* integer multiplication *)
  | DIV             (* integer division *)
  | CEQ             (* compare equal *)
  | CGT             (* compare greater than *)
  | CGTE            (* compare greater than or equal *)
  | ATOM            (* test if value is an integer *)
  | CONS            (* allocate a CONS cell *)
  | CAR             (* extract first element from CONS cell *)
  | CDR             (* extract second element from CONS cell *)
  | SEL of int * int (* conditional branch *)
  | JOIN            (* return from branch *)
  | LDF of int      (* load function *)
  | AP of int       (* call function *)
  | RTN             (* return from function call *)
  | DUM of int      (* create empty environment frame *)
  | RAP of int      (* recursive environment call function *)
  | STOP            (* terminate co-processor execution *)
  (* Tail call extensions *)
  | TSEL of int * int (* tail-call conditional branch *)
  | TAP of int      (* tail-call function *)
  | TRAP of int     (* recursive environment tail-call function *)
  (* Pascal extensions *)
  | ST of int * int (* store to environment *)
  (* Debug extensions *)
  | DBUG            (* printf debugging *)
  | BRK             (* breakpoint debugging *)
  (* virtualこれはなくなる予定 *)
  | L of name (* label *)
  | LLDF of name
  | LSEL of name
  | LTSEL of name
  | IClosure of name * frame
  | IBranch of name * frame * frame
  | C of instr * name (* comment *)
and frame = instr list
and stack = frame list
and environ = (name * frame) list

type register = {
  c : int;    (* control register (program counter / instruction pointer) *)
  s : stack;  (* data stack register *)
  d : stack;  (* control stack register *)
  e : environ list;  (* environment frame register *)
}

let sprintf fmt = Printf.sprintf fmt
let try_fx f x =
  try Some(f x) with
  | _ -> None

let string_of_instrs code =
  let rec string_of_instr op =
    match op with
    | LDC n -> sprintf "LDC %d" n
    | LD(n1, n2) -> sprintf "LD %d %d" n1 n2
    | ADD -> "ADD"
    | SUB -> "SUB"
    | MUL -> "MUL"
    | DIV -> "DIV"
    | CEQ -> "CEQ"
    | CGT -> "CGT"
    | CGTE -> "CGTE"
    | ATOM -> "ATOM"
    | CONS -> "CONS"
    | CAR -> "CAR"
    | CDR -> "CDR"
    | SEL(n1, n2) -> sprintf "SEL %d %d" n1 n2
    | JOIN -> "JOIN"
    | LDF n -> sprintf "LDF %d" n
    | AP n -> sprintf "AP %d" n
    | RTN -> "RTN"
    | DUM n -> sprintf "DUM %d" n
    | RAP n -> sprintf "RAP %d" n
    | STOP -> "STOP"

    | TSEL(n1, n2) -> sprintf "TSEL %d %d" n1 n2
    | TAP n -> sprintf "TAP %d" n
    | TRAP n -> sprintf "TRAP %d" n
    | ST(n1, n2) -> sprintf "ST %d %d" n1 n2

    | DBUG -> "DBUG"
    | BRK -> "BRK"
    (*     // virtualこれはなくなる予定 *)
    | L x -> x ^ ":"
    | LSEL(x) -> "_SEL t_" ^ x ^ " f_" ^ x
    | LTSEL(x) -> "_TSEL t_" ^ x ^ " f_" ^ x
    | LLDF x -> "_LDF " ^ x
    | IClosure(xn, code) ->
      sprintf "IClosure(%s, len=%d)" xn (List.length code) 
    | IBranch(xn,tc,fc) -> 
      sprintf "IBranch(%s, t_len=%d, f_len=%d)"
        xn (List.length tc) (List.length fc)
    | C(x, c) ->
        string_of_instr x ^ "\t; " ^ c
  in
  code
  |> List.map string_of_instr
  |> String.concat "\n"

let rec position_var sym ls =
  let rec loop i = function
    | [] -> None
    | x :: _ when x = sym -> Some i
    | _ :: xs -> loop (i+1) xs
  in
  loop 0 ls
and location sym ls =
  let rec loop i = function
    | [] -> failwith sym (*raise Not_found*)
    | x :: xs ->
      match position_var sym x with
      | Some j -> (i, j)
      | None -> loop (i+1) xs
  in
  loop 0 ls

(* compile main *)
let compile expr =
  let num = ref 0 in
  let gensym x =
    incr num;
    x ^ "." ^ string_of_int !num
  in
  let rec comp env expr =
    match expr with
    | Int n -> [LDC(n)]
    | Add(e1, e2) -> (comp env e1) @ (comp env e2) @ [ADD]
    | Sub(e1, e2) -> (comp env e1) @ (comp env e2) @ [SUB]
    | Mul(e1, e2) -> (comp env e1) @ (comp env e2) @ [MUL]
    | Div(e1, e2) -> (comp env e1) @ (comp env e2) @ [DIV]
    | Eq(e1, e2) ->  (comp env e1) @ (comp env e2) @ [CEQ]
    | Gt(e1, e2) ->  (comp env e1) @ (comp env e2) @ [CGT]
    | GtE(e1, e2) -> (comp env e1) @ (comp env e2) @ [CGTE]
    | Atom(e1) -> (comp env e1) @ [ATOM]
    | Cons(e1, e2) -> (comp env e1) @ (comp env e2) @ [CONS]
    | Car(e1) -> (comp env e1) @ [CAR]
    | Cdr(e1) -> (comp env e1) @ [CDR]
    | Debug(e1) -> let x = comp env e1 in x @ [DBUG] @ x
    | Break(e1) -> (comp env e1) @ [BRK]
    | Var x ->
      let (n,i) = location x env in
      let comment = "ref."^x in
      [C(LD(n,i), comment)] (* 局所変数 *)
    | If(e1, e2, e3) ->
      let t_clause = comp env e2 @ [JOIN] in
      let f_clause = comp env e3 @ [JOIN] in
      let cond = comp env e1 in
      let label = gensym "if" in
      cond @ [LSEL(label)] @ [IBranch(label, t_clause, f_clause)]
    | Lambda(args, e) ->
      let clause = tcomp (args::env) e @ [RTN] in
      let xn = gensym ("fun."^(String.concat "_" args)) in
      LLDF(xn) :: [IClosure(xn, clause)]
    | Let(xe1s, e2) ->
      let (xs, e1s) = xe1s |> List.split in
      let clause = tcomp (xs::env) e2 @ [RTN] in
      let params = e1s |> List.map (comp env) |> List.concat in
      let xn = gensym (String.concat "_" xs) in
      let args = List.length xs in
      params @ (LLDF(xn) :: AP(args) :: [IClosure(xn, clause)]) 
    | LetRec(xe1s, e2) ->
      let (xs, e1s) = xe1s |> List.split in
      let clause = tcomp (xs::env) e2 @ [RTN] in
      let params = e1s |> List.map (comp (xs::env)) |> List.concat in
      let xn = gensym (String.concat "_" xs) in
      let args = List.length xs in
      DUM(args) :: params @ (LLDF(xn) :: RAP(args) :: [IClosure(xn, clause)]) 
    | App(e1, e2) ->
      let param = e2 |> List.map (comp env) |> List.concat in
      let argn = List.length e2 in
      param @ (tcomp env e1) @ [AP(argn)]
    | Tuple(es) ->
      let es_rev = List.rev es in
      let tup = List.fold_left (fun s e -> Cons(e, s)) (List.hd es_rev) (List.tl es_rev) in
      comp env tup
    | LetTuple(pat, e1, e2) ->
      let clause = tcomp (pat::env) e2 @ [RTN] in
      let rec loop acc s = function
        | 2 ->
          (Cdr(s) :: Car(s) :: acc)
          |> List.fold_left (fun s e -> comp env e :: s) []
          |> List.concat
        | n when n > 2 ->
          loop (Car(s)::acc) (Cdr(s)) (n-1)
        | _ -> failwith "LetTuple"
      in
      let param = loop [] e1 (List.length pat) in
      let xn = gensym (String.concat "_" pat) in
      param @ (LLDF(xn) :: AP(List.length pat) :: [IClosure(xn, clause)]) 
  and tcomp env = function
    | Let(xe1s, e2) ->
      let (xs, e1s) = xe1s |> List.split in
      let clause = tcomp (xs::env) e2 in
      let params = e1s |> List.map (comp env) |> List.concat in
      let xn = gensym (String.concat "_" xs) in
      let args = List.length xs in
      params @ (LLDF(xn) :: TAP(args) :: L(xn) :: clause) 
    | LetRec(xe1s, e2) ->
      let (xs, e1s) = xe1s |> List.split in
      let clause = tcomp (xs::env) e2 in
      let params = e1s |> List.map (comp (xs::env)) |> List.concat in
      let xn = gensym (String.concat "_" xs) in
      let args = List.length xs in
      DUM(args) :: params @ (LLDF(xn) :: TRAP(args) :: L(xn) :: clause) 
    | App(e1, e2) ->
      let param = e2 |> List.map (comp env) |> List.concat in
      let argn = List.length e2 in
      param @ (tcomp env e1) @ [TAP(argn)]
    | If(e1, e2, e3) ->
      let t_clause = tcomp env e2 in
      let f_clause = tcomp env e3 in
      let cond = comp env e1 in
      let label = gensym "tif" in
      cond @ (LTSEL(label) :: L("t_"^label) :: t_clause) @ [RTN] 
                           @ (L("f_"^label) :: f_clause) 
    | expr -> comp env expr
  in
  comp [["argv"]] expr @ [RTN]

let closure code =
  let cls = ref [] in
  let rec comp acc = function
    | [] -> List.rev acc
    | IClosure(xn,xc) :: rest ->
      let xc' = L(xn) :: comp [] xc in
      cls := xc'::!cls;
      comp acc rest
    | IBranch(xn,tc,fc) :: rest ->
      let tc' = L("t_"^xn) :: comp [] tc in
      let fc' = L("f_"^xn) :: comp [] fc in
      cls := tc'::fc'::!cls;
      comp acc rest
    | x :: rest ->
      comp (x::acc) rest
  in
  let pass1 = comp [] code in
  let pass2 = List.concat !cls in
  pass1 @ pass2

let emit code =
  let tbl = ref (Hashtbl.create 13) in
  let rec f acc pc = function
    | [] -> List.rev acc
    | L(x) :: rest ->
      Hashtbl.add !tbl x pc;
      f acc pc rest
    | C(x,c) :: rest ->
      f acc pc (x::rest)
    | x :: rest ->
      f (x::acc) (pc+1) rest
  in
  let pass1 = f [] 0 code in
  let rec g acc pc code =
    let find x =
      (Hashtbl.find !tbl) x
    in
    match code with
    | [] -> List.rev acc
    | LLDF(x) :: rest ->
      let fpc = find x in
      g (LDF(fpc)::acc) (pc+1) rest
    | LSEL(x) :: rest ->
      let tpc = find ("t_"^x) in
      let fpc = find ("f_"^x) in
      g (SEL(tpc,fpc)::acc) (pc+1) rest
    | LTSEL(x) :: rest ->
      let tpc = find ("t_"^x) in
      let fpc = find ("f_"^x) in
      g (TSEL(tpc,fpc)::acc) (pc+1) rest
    | x :: rest ->
      g (x::acc) (pc+1) rest
  in
  g [] 0 pass1

