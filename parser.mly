%{
open Syntax
%}

%token<int> INT
%token<string> IDENT
%token FUN ARROW LET REC IN IF THEN ELSE ADD SUB MUL DIV LPAREN RPAREN EQUAL EOF
%token GREATER GREATER_EQUAL LESS LESS_EQUAL ATOM CONS CAR CDR COMMA DBUG BRK
%token AND

%nonassoc IN
%nonassoc LET                           /* above SEMI ( ...; let ... in ...) */
%nonassoc AND             /* above WITH (module rec A: SIG with ... and ...) */
%nonassoc THEN                          /* below ELSE (if ... then ...) */
%nonassoc ELSE                          /* (if ... then ... else ...) */
%right prec_let prec_fun
%right CONS
%right prec_if
%left COMMA
%left EQUAL LESS GREATER LESS_EQUAL GREATER_EQUAL
%left ADD SUB 
%left MUL DIV
%left prec_app

%type <Syntax.t> start
%start start

%%

start:
| exp EOF { $1 }

simple_exp: 
| LPAREN exp RPAREN { $2 }
| INT { Int ($1) }
| IDENT { Var ($1) }

exp:
| simple_exp { $1 }
| exp ADD exp { Add ($1, $3) }
| exp SUB exp { Sub ($1, $3) }
| exp MUL exp { Mul ($1, $3) }
| exp DIV exp { Div ($1, $3) }
| exp EQUAL exp { Eq ($1, $3) }
| exp GREATER exp { Gt ($1, $3) }
| exp GREATER_EQUAL exp { GtE ($1, $3) }
| exp LESS exp { Gt ($3, $1) }
| exp LESS_EQUAL exp { GtE ($3, $1) }
| ATOM exp %prec prec_app { Atom ($2) }
| exp CONS exp { Cons ($1,$3) }
| CAR exp %prec prec_app { Car ($2) }
| CDR exp %prec prec_app { Cdr ($2) }
| DBUG exp %prec prec_app { Debug ($2) }
| BRK exp %prec prec_app { Break ($2) }
| IF exp THEN exp ELSE exp
  %prec prec_if
    { If ($2, $4, $6) }
| FUN formal_args ARROW exp
  %prec prec_fun
    { Lambda ($2, $4) }
| LET let_bindings IN exp
  %prec prec_let
    { Let ($2, $4) }
| LET REC let_bindings IN exp
  %prec prec_let
    { LetRec ($3, $5) }
| exp actual_args
  %prec prec_app
    { App($1, $2) }
| elems
    { Tuple($1) }
| LET LPAREN pat RPAREN EQUAL exp IN exp
    { LetTuple($3, $6, $8) }
| error
    { failwith
        (Printf.sprintf "parse error near characters %d-%d"
           (Parsing.symbol_start ())
           (Parsing.symbol_end ())) }

formal_args:
| IDENT formal_args
    { $1 :: $2 }
| IDENT
    { [$1] }

actual_args:
| actual_args simple_exp
  %prec prec_app
    { $1 @ [$2] }
| simple_exp
  %prec prec_app
    { [$1] }

elems:
| elems COMMA exp
    { $1 @ [$3] }
| exp COMMA exp
    { [$1; $3] }

pat:
| pat COMMA IDENT
    { $1 @ [$3] }
| IDENT COMMA IDENT
    { [$1; $3] }

let_bindings:
    let_binding                  { [$1] }
  | let_bindings AND let_binding { $1 @ [$3] }

let_binding:
  | IDENT EQUAL exp              { ($1, $3) }

