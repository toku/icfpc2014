{
  open Parser
}

let digit = ['0'-'9']
let lower = ['a'-'z']
let upper = ['A'-'Z']
let whitespace = [' ' '\t' ]
let newline = ('\n' | '\r' '\n')

rule token = parse
  | eof { EOF }
  | whitespace { token lexbuf }
  | newline { token lexbuf }
  | "(*"
    { comment lexbuf; (* ネストしたコメントのためのトリック *)
      token lexbuf }
  | '(' { LPAREN }
  | ')' { RPAREN }
  | "+" { ADD }
  | "-" { SUB }
  | "*" { MUL }
  | "/" { DIV }
  | ">=" { GREATER_EQUAL }
  | ">" { GREATER }
  | "<=" { LESS_EQUAL }
  | "<" { LESS }
  | "fun" { FUN }
  | "->" { ARROW }
  | "=" { EQUAL }
  | "let" { LET }
  | "rec" { REC }
  | "in" { IN }
  | "and" { AND }
  | "if" { IF }
  | "then" { THEN }
  | "else" { ELSE }
  | "::" { CONS }
  | "atom" { ATOM }
  | "car" { CAR }
  | "cdr" { CDR }
  | "," { COMMA }
  | "dbug" { DBUG }
  | "brk" { BRK }
  | ['-']?['0'-'9']+ { INT (int_of_string (Lexing.lexeme lexbuf)) }
  | ('_'|lower) (digit|lower|upper|'''|'_')*
    (* 他の「予約語」より後でないといけない *)
    { IDENT(Lexing.lexeme lexbuf) }
  | _
    { failwith
	(Printf.sprintf "unknown token %s near characters %d-%d"
	   (Lexing.lexeme lexbuf)
	   (Lexing.lexeme_start lexbuf)
	   (Lexing.lexeme_end lexbuf)) }
and comment = parse
| "*)"
    { () }
| "(*"
    { comment lexbuf;
      comment lexbuf }
| eof
    { Printf.eprintf "warning: unterminated comment@." }
| _
    { comment lexbuf }

