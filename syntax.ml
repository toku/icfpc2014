type t =
  | Int of int
  | Add of t * t
  | Sub of t * t
  | Mul of t * t
  | Div of t * t
  | Eq of t * t
  | Gt of t * t
  | GtE of t * t
  | Atom of t
  | Cons of t * t
  | Car of t
  | Cdr of t
  | If of t * t * t
  | Var of name
  | Lambda of name list * t
  | Let of (name * t) list * t
  | LetRec of (name * t) list * t
  | App of t * t list
  | Tuple of t list
  | LetTuple of name list * t * t
  | Debug of t
  | Break of t
and name = string
