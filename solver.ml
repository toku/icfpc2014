let rec length = fun l ->
  let rec aux = fun n xs ->
    if atom xs then n
    else aux (n+1) (cdr xs)
  in aux 0 l
and map = fun f xs -> 
  if atom xs then 0
  else (f (car xs), map f (cdr xs))
and map2 = fun f xs ys -> 
  if atom xs then 0
  else (f (car xs) (car ys), map2 f (cdr xs) (cdr ys))
and mapi = fun f xs -> 
  let rec loop = fun i xs ->
    if atom xs then 0
    else (f i (car xs), loop (i+1) (cdr xs))
  in loop 0 xs
and fold = fun f s xs -> 
  if atom xs then s
  else fold f (f s (car xs)) (cdr xs)
and find = fun f xs -> 
  if atom xs then 0
  else if f (car xs) then (car xs) else find f (cdr xs)
and find2 = fun f xs ys ->
  if atom xs then 0
  else if f (car xs) (car ys) then (car xs, car ys)
  else find2 f (cdr xs) (cdr ys)
and append = fun xs ys ->
  rev_append (rev xs) ys
and rev_append = fun l1 l2 ->
  if atom l1 then l2
  else rev_append (cdr l1) (car l1, l2)
and rev = fun l ->
  rev_append l 0
and rev_filter = fun f xs -> 
  let rec loop = fun acc xs ->
    if atom xs then acc
    else if f (car xs) then loop (car xs, acc) (cdr xs)
    else loop acc (cdr xs)
  in loop 0 xs
and filter = fun f xs -> 
  rev (rev_filter f xs)
and heap_create = fun cmp ->
  (cmp, 0)
and heap_meld = fun cmp a b ->
  if atom a then b
  else if atom b then a
  else
    let merge = fun a b ->
      let (a_val, a_l, a_r) = a in
      let a_r = heap_meld cmp a_r b in
      (a_val, a_r, a_l) (* swap a.l a.r *)
    in
    if cmp (car a) (car b) then
      merge b a
    else
      merge a b
and heap_push = fun v h ->
  let (cmp, root) = h in
  let p = (v, 0, 0) in
  let root = heap_meld cmp root p in
  (cmp, root)
and heap_empty = fun h ->
  atom (cdr h)
and heap_top = fun h ->
  car (cdr h)
and heap_pop = fun h ->
  let (cmp, root) = h in
  let (_, root_l, root_r) = root in
  let root = heap_meld cmp root_r root_l in
  (cmp, root)

and _R = 0
and _B = 1
and _E = 0
and tree_create = fun cmp ->
  (cmp, _E)

(*
member x E = False
member x (T _ a y b)
    | x < y  = member x a
    | x > y  = member x b
    | True   = True
 *)
and tree_member = fun x tree ->
  let (cmp, root) = tree in
  let rec mem = fun ayb ->
    if atom ayb then 0
    else
      let (_, a, y, b) = ayb in
      if cmp x y then mem a
      else if cmp y x then mem b
      else 1
  in
  mem root

(*
insert x s = T B a y b
  where 
    T _ a y b = ins s
    ins E = T R E x E
    ins s@(T color a y b) 
      | x < y  = balance color (ins a) y b
      | x > y  = balance color a y (ins b)
      | True   = s
 *)
and tree_insert = fun x tree ->
  let (cmp, root) = tree in
  let rec ins = fun ayb ->
    if atom ayb then (_R, _E, x, _E)
    else
      let (col, a, y, b) = ayb in
      if cmp x y then tree_balance col (ins a) y b
      else if cmp y x then tree_balance col a y (ins b)
      else ayb
  in
  let (_, ayb) = ins root in
  (cmp, (_B, ayb))

(*
balance B (T R (T R a x b) y c) z d = T R (T B a x b) y (T B c z d)
balance B (T R a x (T R b y c)) z d = T R (T B a x b) y (T B c z d)
balance B a x (T R (T R b y c) z d) = T R (T B a x b) y (T B c z d)
balance B a x (T R b y (T R c z d)) = T R (T B a x b) y (T B c z d)
balance color a x b = T color a x b
 *)
and tree_balance = fun color left key right ->
  let balance_1 = fun axbyc z d ->
    if color = _R then 0 else
    if atom axbyc then 0
    else
      let (col, axb, y, c) = axbyc in
      if col = _R then
        if atom axb then 0
        else
          let (col, a, x, b) = axb in
          if col = _R then
            (_R, (_B, a, x, b), y, (_B, c, z, d))
          else 0
      else 0
  and balance_2 = fun axbyc z d ->
    if color = _R then 0 else
    if atom axbyc then 0
    else
      let (col, a, x, byc) = axbyc in
      if col = _R then
        if atom byc then 0
        else
          let (col, b, y, c) = byc in
          if col = _R then
            (_R, (_B, a, x, b), y, (_B, c, z, d))
          else 0
      else 0
  and balance_3 = fun a x byczd ->
    if color = _R then 0 else
    if atom byczd then 0
    else
      let (col, byc, z, d) = byczd in
      if col = _R then
        if atom byc then 0
        else
          let (col, b, y, c) = byc in
          if col = _R then
            (_R, (_B, a, x, b), y, (_B, c, z, d))
          else 0
      else 0
  and balance_4 = fun a x byczd ->
    if color = _R then 0 else
    if atom byczd then 0
    else
      let (col, b, y, czd) = byczd in
      if col = _R then
        if atom czd then 0
        else
          let (col, c, z, d) = czd in
          if col = _R then
            (_R, (_B, a, x, b), y, (_B, c, z, d))
          else 0
      else 0
  and balance_5 = fun a x b ->
    (color, a, x, b)
  in
  let rec match_with = fun p l ->
    if atom l then dbug 0
    else
      let ans = p (car l) in
      if atom ans then match_with p (cdr l)
      else ans
  in
  let patterns = (balance_1,balance_2,balance_3,balance_4,balance_5,0) in
  match_with (fun f -> f left key right) patterns

and sort = fun cmp l ->
  let h = heap_create cmp in
  let h = fold (fun h i -> heap_push i h) h l in
  let rec loop = fun acc h ->
    if heap_empty h then acc
    else
      let v = heap_top h
      and h = heap_pop h
      in loop (v,acc) h
  in
  loop 0 h
and for_all = fun p l ->
  if atom l then 1
  else if p (car l) then for_all p (cdr l)
  else 0
and exists = fun p l ->
  if atom l then 0
  else if p (car l) then 1
  else exists p (cdr l)
and for_all2 = fun p l1 l2 ->
  if atom l1 then 1
  else if p (car l1) (car l2) then for_all2 p (cdr l1) (cdr l2)
  else 0
and exists2 = fun p l1 l2 ->
  if atom l1 then 0
  else if p (car l1) (car l2) then 1
  else exists2 p (cdr l1) (cdr l2)
and eq_all = fun xs ys ->
  if atom xs then
    if atom ys then xs = ys else 0
  else
    if atom ys then 0
    else
      let (xa, xb) = xs in
      let (ya, yb) = ys in
      if eq_all xa ya then eq_all xb yb else 0
and getnext = fun d ->
  if d >= 3 then 0
  else d + 1
and eq_pair = fun lhs rhs ->
  let l1 = car lhs
  and l2 = cdr lhs
  and r1 = car rhs
  and r2 = cdr rhs in
  if l1 = r1 then l2 = r2 else 0
and cmp_pair = fun a b ->
  let (ax, ay) = a in
  let (bx, by) = b in
  if ax < bx then 1
  else if ax = bx then ay < by
  else 0
and abs = fun x ->
  if x < 0 then (-1 * x) else x
and distance = fun a b ->
  let (ax,ay) = a in
  let (bx,by) = b in
  abs (ax-bx) + abs (ay-by)
and shortest_path = fun walls start goal ->
  let rec bfs = fun next visited ->
    if heap_empty next then 0 else
    let top = heap_top next in
    let (score, cur, dir_rev) = top in
    if eq_pair cur goal then
      rev dir_rev
    else
      let next = heap_pop next in
      let rec loop = fun i next ->
        if i > 3 then next
        else
          let moved = move walls cur i in
          if eq_pair moved cur then
            loop (i + 1) next
          else if exists (fun x -> eq_pair moved x) visited then
              loop (i + 1) next
          else
            let next =
              let score = distance moved goal in
              heap_push (score, moved, (i,dir_rev)) next
            in
            loop (i + 1) next
      in
      let next = loop 0 next in
      bfs next (cur,visited)
(*
  and move = fun cur dir ->
    let (x,y) = cur in
    let moved =
      if dir = 0 then (x, y - 1)
      else if dir = 1 then (x + 1, y)
      else if dir = 2 then (x, y + 1)
      else if dir = 3 then (x - 1, y)
      else cur
    in
    if tree_member moved walls then cur
    else moved
 *)
  in
  let h = heap_create (fun a b -> car a > car b) in
  let h = heap_push (0,start,0) h in
  bfs h 0
and step = fun state world ->
  let (path, param) = state in
  let (walls, pill, ppill, fruit) = param in
  let (fruit_loc, prev_fruit_expire) = fruit in
  let (field, lambdaman, ghosts, fruit_expire) = world in
(*     let (vit, loc, dir, lives, score) = lambdaman in *)
  let (_, loc, _) = lambdaman in
  let rm_f = fun x -> if eq_pair x loc then 0 else 1 in
  let pill = filter rm_f pill in
  let ppill = filter rm_f ppill in
  let param = (walls, pill, ppill, (fruit_loc, fruit_expire)) in
  let escape_from_ghosts =
    let ghost = find (fun i ->
                    let (gvit, gloc, gdir) = i in
                    if gvit = 1 then 0 else
                    let dist = distance loc gloc in
                    if dist > 3 then 0 else
                    let dist = length (shortest_path walls loc gloc) in
                    dist <= 3) ghosts in
    if atom ghost then 0
    else
      let (gvit, gloc, gdir) = ghost in
      let rec loop = fun i dist dir ->
        if i > 3 then dir
        else
          let moved = move walls loc i in
          if eq_pair moved loc then
            loop (i + 1) dist dir
          else
            let score = length (shortest_path walls moved gloc) in
            if score > dist then
              loop (i + 1) score i
            else 
              loop (i + 1) dist dir
      in
      let dir = loop 0 0 gdir in
      let _ = dbug (gloc, dir) in
      let param = (walls, pill, ppill, (fruit_loc, 0)) in
      ((0, param), dir)
  in
  if if atom escape_from_ghosts then 0 else 1 then
    escape_from_ghosts
  else
  let catch_up_fruits =
    if fruit_expire > 0 then prev_fruit_expire = 0
    else 0
  in
  if catch_up_fruits then
    let path = shortest_path walls loc fruit_loc in
    let _ = dbug (loc, path) in
    ((cdr path, param), car path)
  else if atom path then
    let targets = if atom ppill then pill else ppill in
    let h = heap_create (fun a b -> car a > car b) in
    let h = fold (fun h i -> let x = (distance loc i, i) in heap_push x h) h targets in
    let goal = cdr (heap_top h) in
    let path = shortest_path walls loc goal in
    ((cdr path, param), car path)
  else
    ((cdr path, param), car path)
and make_param = fun field ->
  let rec loop = fun wall pill ppill fruit y x field_y field_x ->
    if atom field_x then
      let (_, next) = field_y in
      if atom next then
        (wall, pill, ppill, fruit)
      else
        loop wall pill ppill fruit (y+1) 0 next (car next)
    else
      let (item,rest) = field_x in
      if item = 0 then
        loop (tree_insert (x,y) wall) pill ppill fruit y (x+1) field_y rest
      else if item = 2 then
        loop wall ((x,y), pill) ppill fruit y (x+1) field_y rest
      else if item = 3 then
        loop wall pill ((x,y), ppill) fruit y (x+1) field_y rest
      else if item = 4 then
        loop wall pill ppill ((x,y),0) y (x+1) field_y rest
      else 
        loop wall pill ppill fruit y (x+1) field_y rest
  in
  let wall_tree = tree_create cmp_pair in
  loop wall_tree 0 0 0 0 0 field (car field)
and move = fun walls cur dir ->
  let (x,y) = cur in
  let moved =
    if dir = 0 then (x, y - 1)
    else if dir = 1 then (x + 1, y)
    else if dir = 2 then (x, y + 1)
    else if dir = 3 then (x - 1, y)
    else cur
  in
  if tree_member moved walls then cur
  else moved
in
let (field, lambdaman, ghosts, fruits) = argv in
let (_, loc, _) = lambdaman in
let param = make_param field in
let (walls, pill, ppill, fruit) = param in
(*
let tr = tree_create cmp_pair in
let tr = fold (fun s i -> tree_insert i s) tr walls in
let _ = dbug (tree_member (0,0) tr) in
let _ = dbug (tree_member (10,10) tr) in
let _ = dbug tr in
 *)
(*
let datas = (wall, pill, ppill, 0) in
let sorted = map (fun x -> sort cmp_pair x) datas in
let (wall, pill, ppill, _) = sorted in
let _ = dbug wall in
 *)
let goal = car ppill in
let _ = dbug goal in
let path = shortest_path walls loc goal in
let _ = dbug path in
((path,param),step)
